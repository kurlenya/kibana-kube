FROM gcr.io/google_containers/ubuntu-slim:0.4

ENV DEBIAN_FRONTEND noninteractive
ENV KIBANA_VERSION 5.0.2

RUN apt-get update \
    && apt-get install -y curl \
    && apt-get clean

RUN set -x \
    && cd / \
    && mkdir /kibana \
    && curl -O https://artifacts.elastic.co/downloads/kibana/kibana-$KIBANA_VERSION-linux-x86_64.tar.gz \
    && tar xf kibana-$KIBANA_VERSION-linux-x86_64.tar.gz -C /kibana --strip-components=1 \
    && rm kibana-$KIBANA_VERSION-linux-x86_64.tar.gz

COPY run.sh /run.sh
RUN chmod +x /run.sh

EXPOSE 5601

CMD ["/run.sh"]
